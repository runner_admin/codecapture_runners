We're roughly following the recipe outlined here:

http://clouddocs.web.cern.ch/clouddocs/containers/tutorials/swarmgitlab.html

1. log in to `lxplus-cloud` and source your RC file
2. if you haven't yet, create a new cluster template
   
   this template will be mostly the same as `swarm` but have a larger
   flavor and docker volume

   Note: update the image id
   Note: make sure you have enought storage or adjust the docker volume size
   
   magnum cluster-template-show swarm|grep image_id

    magnum cluster-template-create \  
    --name codecap-swarm-large \
    --labels cvmfs_tag=cvmfs \
    --master-flavor-id m2.large \
    --external-network-id CERN_NETWORK \
    --image-id <image id> \ 
    --volume-driver rexray \
    --flavor-id m2.large \
    --dns-nameserver 137.138.17.5 \
    --coe swarm --docker-volume-size 300




    magnum cluster-create --name gitlabci-swarm --keypair-id openstack --cluster-template codecap-swarm-large --node-count 1

    mkdir magnum && cd magnum
    magnum cluster-config gitlabci-swarm > setup.sh
    source setup.sh


    docker pull gitlab/gitlab-runner

    docker volume create --name gitlab-runner-config
    docker volume create -d cvmfs --name=atlas-condb.cern.ch
    docker volume create -d cvmfs --name=atlas.cern.ch
    docker volume create -d cvmfs --name=sft.cern.ch


    docker run -d --name datavol -v gitlab-runner-config:/etc/gitlab-runner busybox tail -f /dev/null
    docker cp ca.pem datavol:/etc/gitlab-runner
    docker cp cert.pem datavol:/etc/gitlab-runner
    docker cp key.pem datavol:/etc/gitlab-runner


    echo $DOCKER_HOST #we'll use this later in the register command

    docker run -d --restart always --name gitlab-runner -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner
    docker exec -it  gitlab-runner bash


    # get the runner token at https://gitlab.cern.ch/runner_admin/runner_admin/settings/ci_cd

    gitlab-ci-multi-runner register -r <your GitLab CI runner token> \
    --name codecapture-runners --tag-list code-capture --limit 1 \
    --url https://gitlab.cern.ch/ci --executor docker \
    --docker-host <the docker host from grep above> --docker-image centos:7 \
    --docker-disable-cache --docker-cert-path /etc/gitlab-runner

this command created a configuration file called `config.toml` that we need to edit a bit more
so let's copy it back to lxplus:

    docker cp datavol:/etc/gitlab-runner/config.toml newconfig.toml
    
edit config.toml

    volumes = ["/cache","/var/run/docker.sock:/var/run/docker.sock"]
    tls_verify = true 
    privileged = true

copy back the config

    docker cp newconfig.toml datavol:/etc/gitlab-runner/config.toml

you can check back on the status of the runner via (it should automatically re-load the config)

    docker logs -f gitlab-runner 


on your GitLab CI page you should see the runner being available. (on Settings / Pipelines)